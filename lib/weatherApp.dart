import 'package:flutter/material.dart';


void main() {
  runApp(climatePage());
}


class climatePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          // backgroundColor: Colors.white,
          extendBodyBehindAppBar: true,
          body: buildBodyWidget()
          ),
    );
  }
}
Widget buildBodyWidget(){
  return ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                            width: double.infinity,
                            height: 380,
                            child: Image.network(
                              "https://www.i-pic.info/i/D5lR335283.png",
                              fit: BoxFit.cover,
                            )),
                        Container(
                            height: 40,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "  บ้านหนองกระเสริม",
                                  style: TextStyle(
                                      color: Colors.white,
                                      height: 0,
                                      fontSize: 25),
                                ),
                              ],
                            )),
                        Container(
                            height: 80,
                            width: double.infinity,
                            child: Align(
                                alignment: Alignment.topRight,
                                child: Column(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.more_vert,
                                        color: Colors.white,
                                      ),
                                      onPressed: () {},
                                    ),
                                  ],
                                ))),
                        Container(
                          height: 350,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Text(
                                  "\n\n29",
                                  style: TextStyle(fontSize: 90),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(0.0),
                                child: Text(
                                  "\n\n\n\n\n\n °C\n มีเมฆเป็นบางส่วน",
                                  style: TextStyle(fontSize: 30),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 380,
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(18.0),
                                child: Text(
                                  "31 ธ.ค. ส. 29°C/20°C",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      height: 25),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 6, bottom: 6),
                      child: IconTime(),
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Container(
                      child: IconTimeDay(),
                    ),
                    Center(
                      child: OutlinedButton(
                        onPressed: () => {},
                        child: Text(
                          'พยากรณ์สภาพอากาศในช่วงเวลา 15 วัน',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      height: 100,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "รายละเอียดสภาพอากาศ",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 100,
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: details1(),
                    ),
                    Container(
                      height: 100,
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: details2(),
                    ),
                    Container(
                      height: 100,
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: details3(),
                    ),
                    Container(
                      height: 30,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "63160201 Narachai Praphaisan",
                            style: TextStyle(fontSize: 15, color: Colors.grey),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            );
}

Widget details1() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      details1_1(),
      details1_2(),
    ],
  );
}

Widget details1_1() {
  return Column(
    children: <Widget>[
      Text(
        "อุณหภูมิที่ร่างกายรู้สึกได้     ",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        "31°C",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget details1_2() {
  return Column(
    children: <Widget>[
      Text(
        "ความชื้น            ",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        " 51%",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget details2() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      details2_1(),
      details2_2(),
    ],
  );
}

Widget details2_1() {
  return Column(
    children: <Widget>[
      Text(
        "ลมทิศตะวันออกเฉียงเหนือ",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        "16 กม./ชม.",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget details2_2() {
  return Column(
    children: <Widget>[
      Text(
        "ยูวี",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        "อ่อนมาก",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget details3() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      details3_1(),
      details3_2(),
    ],
  );
}

Widget details3_1() {
  return Column(
    children: <Widget>[
      Text(
        "ทัศนวิสัย                                  ",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        "14 กม.",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget details3_2() {
  return Column(
    children: <Widget>[
      Text(
        "ความกดอากาศ",
        style: TextStyle(fontSize: 18),
        textAlign: TextAlign.center,
      ),
      Text(
        "1014hPa",
        style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget IconTimeDay() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: <Widget>[
      IconTimeDay1(),
      IconTimeDay2(),
      IconTimeDay3(),
      IconTimeDay4(),
      IconTimeDay5(),
    ],
  );
}

Widget IconTimeDay1() {
  return Row(
    children: <Widget>[
      Text(
        "    วันนี้                               ",
        style: TextStyle(fontSize: 17),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_outlined,
        ),
        onPressed: () {},
      ),
      Text(
        "มีเมฆเป็นบาง...     29/20°C",
        style: TextStyle(fontSize: 17),
      ),
    ],
  );
}

Widget IconTimeDay2() {
  return Row(
    children: <Widget>[
      Text(
        "    พรุ่งนี้                             ",
        style: TextStyle(fontSize: 17),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text(
        "โคตรร้อนทุก...     33/23°C",
        style: TextStyle(fontSize: 17),
      ),
    ],
  );
}

Widget IconTimeDay3() {
  return Row(
    children: <Widget>[
      Text(
        "    จ.                                   ",
        style: TextStyle(fontSize: 17),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_off_sharp,
        ),
        onPressed: () {},
      ),
      Text(
        "เมฆปิดให้บริ...      29/20°C",
        style: TextStyle(fontSize: 17),
      ),
    ],
  );
}

Widget IconTimeDay4() {
  return Row(
    children: <Widget>[
      Text(
        "    อ.                                   ",
        style: TextStyle(fontSize: 17),
      ),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
        ),
        onPressed: () {},
      ),
      Text(
        "มีฝนบางแห่ง...      29/20°C",
        style: TextStyle(fontSize: 17),
      ),
    ],
  );
}

Widget IconTimeDay5() {
  return Row(
    children: <Widget>[
      Text(
        "    พ.                                   ",
        style: TextStyle(fontSize: 17),
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text(
        "วันโลกแตกRI...    80/50°C",
        style: TextStyle(fontSize: 17),
      ),
    ],
  );
}

Widget IconTime() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      IconTime1(),
      IconTime2(),
      IconTime3(),
      IconTime4(),
      IconTime5(),
      IconTime6()
    ],
  );
}

Widget IconTime1() {
  return Column(
    children: <Widget>[
      Text(
        "16.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_outlined,
        ),
        onPressed: () {},
      ),
      Text(
        "29°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget IconTime2() {
  return Column(
    children: <Widget>[
      Text(
        "17.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_off,
        ),
        onPressed: () {},
      ),
      Text(
        "28°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget IconTime3() {
  return Column(
    children: <Widget>[
      Text(
        "18.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_download,
        ),
        onPressed: () {},
      ),
      Text(
        "27°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget IconTime4() {
  return Column(
    children: <Widget>[
      Text(
        "19.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_done,
        ),
        onPressed: () {},
      ),
      Text(
        "28°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget IconTime5() {
  return Column(
    children: <Widget>[
      Text(
        "20.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloudy_snowing,
        ),
        onPressed: () {},
      ),
      Text(
        "27°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}

Widget IconTime6() {
  return Column(
    children: <Widget>[
      Text(
        "21.00 น.",
        style: TextStyle(fontSize: 16),
      ),
      IconButton(
        icon: Icon(
          Icons.cloud_sync_sharp,
        ),
        onPressed: () {},
      ),
      Text(
        "-10°C",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ],
  );
}